# Enviar correos electrónicos con Mailgun y AdonisJS

Resultado del tutorial: [#008 - Enviar correos electrónicos con Mailgun y AdonisJS](http://www.victorvr.com/tutorial/enviar-correos-electronicos-con-mailgun-y-adonisjs)

Demo: [https://adonisjs-mailgun.herokuapp.com](https://adonisjs-mailgun.herokuapp.com)

![App](/public/app.gif)

## Requerimientos

Esta aplicación asume que tienes lo siguiente instalado en tu computadora:

`node >= 8.0` o mayor.

```bash
node --version
```

`npm >= 5.0` o mayor.

```bash
npm --version
```

Credenciales de la [API de Mailgun](https://www.mailgun.com/) `DOMAIN` y `API_KEY`.

## Instalación de Adonis CLI

Primero necesitamos instalar `Adonis CLI`:

```bash
npm i -g adonisjs-cli
```

## Instalación de Dependencias via NPM

Ahora instalaremos las dependencias de nuestra aplicación:

```bash
npm install
```

## Configurar variables de entorno

Configuramos las siguientes variables de entorno en el archivo `.env`:

```bash
MAIL_CONNECTION=mailgun
MAILGUN_DOMAIN={YOUR_MAILGUN_DOMAIN}
MAILGUN_API_KEY={YOUR_MAILGUN_API_KEY}
```

## Iniciar el servidor
Ejecutamos la aplicación:

```bash
adonis serve --dev
```

## Abrir la aplicación
Y por último, abrimos [http://localhost:3333](http://localhost:3333) en el navegador.