//app/Controllers/Http/HomeController.js
'use strict'

const Mail = use('Mail')
const Helpers = use('Helpers')
const moment = use('moment')

class HomeController {

  index ({ view }) {

    return view.render('index')

  }

  async send ({ request, response, session }) {

    var mail = request.input('mail') || ''
    if(mail.trim() != '') {
      const data = {
        to: {
          mail: mail
        },
        from: {
          mail: 'adonisjs@mycompany.com',
          name: 'AdonisJS Demo - MailGun'
        },
        subject: 'Purchase details for #XYZ-123',
        date: moment().format('MMMM Do YYYY, HH:mm:ss'),
        company: {
          name: 'My Company',
          website: 'http://support.mycompany.com'
        },
        book: {
          sku: 'P001',
          title: 'Build Apps with Adonis.JS',
          price: 5,
          currency: 'USD'
        }
      }
      try{
        await Mail.send('mails.purchase', data, (message, error) => {
          message
            .to(data.to.mail)
            .from(data.from.mail, data.from.name)
            .subject(data.subject)
            .embed(Helpers.publicPath('pyramid.png'), 'logo', {
              filename: 'Logo.png'
            })
            .attach(Helpers.resourcesPath('files/Book-' + data.book.sku + '.pdf'), {
              filename: 'Book-' + data.book.sku + '.pdf'
            })
        })
        session.flash({
          notification_class: 'alert-success',
          notification_icon: 'fa-check',
          notification_message: 'The purchase details have been sent to <b>' + mail + '</b>. Enter to your inbox to see the details.'
        })
      }
      catch(ex) {
        var error = ex.errors
        try {
          var json = JSON.parse(ex.errors)
          error = json.message
        } catch (e) {}
        session.flash({
          notification_class: 'alert-danger',
          notification_icon: 'fa-times',
          notification_message: 'ERROR - ' + error
        })
      }
    }
    else {
      session.flash({
        notification_class: 'alert-warning',
        notification_icon: 'fa-warning',
        notification_message: 'Warning - Mail empty. Try again.'
      })
    }
    return response.redirect('/')

  }

}

module.exports = HomeController